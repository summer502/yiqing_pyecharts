import time
import json
import requests
import pandas as pd

url="https://view.inews.qq.com/g2/getOnsInfo?name=disease_h5&callback=&_=%d"%int(time.time()*1000)

##模拟浏览器发送请求，接受响应
resp=requests.get(url)
# print(resp.json()['data'])
# print(type(resp.json()['data']))
data=json.loads(resp.json()['data'])  #response.json() 是把返回响应的json字符串转换成字典。 response.json() 等同于 json.loads(response.text)
# data=resp.json()['data']  #json.loads()函数是将json格式数据转换为字典（可以这么理解，json.loads()函数是将字符串转化为字典）
china_data=data['areaTree'][0]['children']

print(type(data))
print(china_data)
# print(data['areaTree'][0])   #中国数据
#
print(type(china_data))
data_set=[]
print(type(data_set)) #list
for i in china_data:
    data_dict={} #dict
    #地区
    data_dict['province']=i['name']#每个字典name对应的内容
    #疫情数据
    #新增确诊
    data_dict['nowConfirm'] = i['total']['nowConfirm']
    #累计确诊人数
    data_dict['confirm']=i['total']['confirm']
    #死亡人数
    data_dict['dead']=i['total']['dead']
    #治愈人数
    data_dict['heal']=i['total']['heal']
    #死亡率
    data_dict['deadRate'] = i['total']['deadRate']
    #治愈率
    data_dict['healRate'] = i['total']['healRate']
    data_set.append(data_dict)#每个元素对应一个省份
df=pd.DataFrame(data_set)  #liebiao
print(df)
#4.保存数据csv
df.to_csv('./国内疫情数据.csv')