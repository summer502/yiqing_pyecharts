import pandas as pd
from pyecharts import options as opts
from pyecharts.charts import Map

fp = pd.read_csv('国内疫情数据.csv', encoding='utf-8')
# print(fp)
province = list(fp.province)  # 词
# print(province)
confirm = list(fp.confirm)
# print(confirm)
list = list(zip(province, confirm))
map = (
    # 初始化配置项 初始化宽高还有网页标题
    Map(opts.InitOpts(width='1200px', height='600px', page_title="中国疫情确诊人数地图"))
        .add("中国地图", list, "china")
    # 全局配置项
        .set_global_opts(
            #标题配置项
            title_opts=opts.TitleOpts(title="Map-中国疫情确诊人数示例"),
            #视觉映射配置项   https://blog.csdn.net/qq_42374697/article/details/105381277
            visualmap_opts=opts.VisualMapOpts(
        # max_=70000,is_piecewise=True
        pieces=[
            {"value": 0, "label": "无", "color": "#00ccFF"},
            {"min": 1, "max": 9, "color": "#FFCCCC"},
            {"min": 10, "max": 99, "color": "#DB5A6B"},
            {"min": 100, "max": 499, "color": "#FF6666"},
            {"min": 500, "max": 999, "color": "#CC2929"},
            {"min": 1000, "max": 9999, "color": "#8C0D0D"},
            {"min": 10000, "color": "#9d2933"}
        ],
        is_piecewise=True #设置为分段
    )
                         )
)
print(type(map))
map.render(path='中国疫情地图.html') # 画出地图保存在当前路径下
