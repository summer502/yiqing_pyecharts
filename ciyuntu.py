import pandas as pd
from pyecharts import options as opts
from pyecharts.charts import WordCloud


fp = pd.read_csv('国内疫情数据.csv', encoding='utf-8')
# print(list(fp.province))
# print(list(fp.confirm))
# data_list = (list(fp.province),list(fp.confirm))
data_list1 =zip(list(fp.province),list(fp.confirm))
# print(type(data_list1))
new_data=list(data_list1)
# print(type(new_data))

c=(
    WordCloud(init_opts=opts.InitOpts(page_title="中国疫情词云图"))
    .add(series_name="中国疫情词云图", data_pair=new_data, word_size_range=[50, 400])
    .set_global_opts(
        title_opts=opts.TitleOpts(
            title="中国疫情词云图", title_textstyle_opts=opts.TextStyleOpts(font_size=23)
        ),
        tooltip_opts=opts.TooltipOpts(is_show=True),
    )

)
c.render(path='中国疫情词云图.html')


#遇到问题：https://blog.csdn.net/qq_24918869/article/details/52080163  https://www.jb51.net/article/149359.htm