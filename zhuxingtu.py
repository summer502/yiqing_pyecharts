from pyecharts.charts import Bar
from pyecharts import options as opts
import pandas as pd

fp = pd.read_csv('国内疫情数据.csv', encoding='utf-8')
province = list(fp.province)  # 词
healRate = list(fp.healRate)  # 词
deadRate = list(fp.deadRate)  # 词
print(province)
print(healRate)
print(deadRate)

# V1 版本开始支持链式调用
# num = df2['deadRate'].values.tolist()
# num1=df2['healRate'].values.tolist()
# lab = df2['province'].values.tolist()
# print(num)
# print(num1)
bar = (
    Bar(
            opts.InitOpts(width='1600px',height='600px',page_title="疫情治愈率和死亡率情况")
    )
    .add_xaxis(province)
    # .add_yaxis(num)
    .add_yaxis("治愈率", healRate)
    .add_yaxis("死亡率", deadRate)
    .set_global_opts(title_opts=opts.TitleOpts(title="疫情治愈率和死亡率情况"))
)
bar.render("./柱状图.html")