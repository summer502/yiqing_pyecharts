import pandas as pd
from pyecharts.charts import Line
from pyecharts import options as opts

fp = pd.read_csv('国内近两个月新增确诊数数据.csv', encoding='utf-8')
# num=df['confirm'].values.tolist()
# lab=df['date'].values.tolist()
date = list(fp.date)  # 词
newdate=[str(i) for i in date]#没有这个折线图会乱
confirm=list(fp.confirm)
print(newdate)
print(confirm)
(
    Line(init_opts=opts.InitOpts(width='1720px', height='720px',page_title="中国疫情折线图")) #设置画布大小
    .add_xaxis(newdate)
    .add_yaxis('中国新增病例',confirm)
    .set_global_opts(title_opts=opts.TitleOpts(title="中国疫情基本示例"))
).render(path='中国疫情折线图.html')
