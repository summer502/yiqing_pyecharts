import pandas as pd
from pyecharts import options as opts
from pyecharts.charts import Pie
fp = pd.read_csv('国内疫情数据.csv', encoding='utf-8')
province = list(fp.province)  # 词
# print(province)
confirm=list(fp.confirm)
# print(confirm)
(
    Pie(init_opts=opts.InitOpts(width='1440px',height='920px',page_title="中国疫情确诊人数环图"))#默认900，600
    # .add(series_name='', data_pair=[(j, i) for i, j in zip(num, lab)])#饼图
    .add(series_name='',data_pair=[(j,i) for i,j in zip(confirm,province)],radius=['40%','75%'])#环图
    #.add(series_name='', data_pair=[(j, i) for i, j in zip(num, lab)], rosetype='radius')#南丁格尔图
).render("./环图.html")
# https://www.cnblogs.com/jinjiangongzuoshi/p/12842381.html pyecharts 教学


# json.loads()和response.json()的区别
# https://blog.csdn.net/qq_45487813/article/details/104437189